
public class DeclaracionVariablesNumericasApp {

	public static void main(String[] args) {
		
		// Declaración de cuatro variables numéricas
		int X = 1;
		int Y = 2;
		double N = 3.45;
		double M = 6.78;
		
		// Muestra por consola de variables
		System.out.println(X);
		System.out.println(Y);
		System.out.println(N);
		System.out.println(M);
		
		// Muestra por consola de operaciones
		System.out.println(X + Y);
		System.out.println(X - Y);
		System.out.println(X * Y);
		System.out.println(X / Y);
		System.out.println(X % Y);
		System.out.println(N + M);
		System.out.println(N - M);
		System.out.println(N * M);
		System.out.println(N / M);
		System.out.println(N % M);
		System.out.println(X + N);
		System.out.println(Y / M);
		System.out.println(Y % M);
		
		// Muestra por consola del doble de cada variable
		System.out.println(X + X);
		System.out.println(Y + Y);
		System.out.println(N + N);
		System.out.println(M + M);
		
		// Muestra por consola de la suma de todas las variables
		System.out.println(X + Y + N + M);
		
		// Muestra por consola del producto de todas las variables
		System.out.println(X * Y * N * M);
		
	}

}
